USE_CAMERA_STUB := true

# inherit from the proprietary version
-include vendor/tcl/s960/BoardConfigVendor.mk

#CPU
TARGET_ARCH= arm
TARGET_NO_BOOTLOADER := true
TARGET_BOARD_PLATFORM := MTK65XX

TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
ARCH_ARM_HAVE_TLS_REGISTER := true
TARGET_BOOTLOADER_BOARD_NAME := s960

#KERNEL
BOARD_KERNEL_BASE := 0x10000000
BOARD_KERNEL_PAGESIZE := 2048


# fix this up by examining /proc/mtd on a running device
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x805c0000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x60000000
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 0x805c0000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 0x805c0000
BOARD_FLASH_BLOCK_SIZE := 131072

TARGET_PREBUILT_KERNEL := device/tcl/s960/kernel

#recovery
# Use this flag if the board has a ext4 partition larger than 2gb
BOARD_USES_MMCUTILS := true
BOARD_HAS_NO_MISC_PARTITION := true
BOARD_HAS_NO_SELECT_BUTTON := true

#repack mtk recovery
TOOLS_DIR := device/tcl/s960/mtktools
RECOVERY_ROOT_DIR := out/target/product/s960/recovery/root 
BOARD_CUSTOM_BOOTIMG_MK := device/tcl/s960/mkbootimg.mk    
    

