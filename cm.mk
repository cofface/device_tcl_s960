## Specify phone tech before including full_phone
$(call inherit-product, vendor/cm/config/gsm.mk)

# Release name
PRODUCT_RELEASE_NAME := s960

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/tcl/s960/device_s960.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := s960
PRODUCT_NAME := cm_s960
PRODUCT_BRAND := tcl
PRODUCT_MODEL := tcl s960
PRODUCT_MANUFACTURER := tcl
