$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_us_supl.mk)

PRODUCT_COPY_FILES += \
 $(LOCAL_PATH)/res/images:$(TARGET_RECOVERY_ROOT_OUT)/res/images

PRODUCT_DEFAULT_PROPERTY_OVERRIDES := \
ro.weibo.com=weibo.com/cofface

$(call inherit-product, build/target/product/full.mk)
